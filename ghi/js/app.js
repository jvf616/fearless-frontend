function createCard(name, description, pictureUrl, startDate, endDate, location) {
    return `
    <div class = "col">
     <div class="shadow p-3 mb-5 bg-body rounded">
      <div class="card">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
          <p class="card-text">${description}</p>
        </div>
        <div class="card-footer">${startDate} - ${endDate}</div>
      </div>
     </div>
    </div>
    `;
  }

function errorAlert(){
    return `
    <div class="alert alert-primary" role="alert">
        Currently having database issues. We're working as fast as possible to fix it!
    </div>
    `
}
window.addEventListener('DOMContentLoaded', async () => {
    
const url = 'http://localhost:8000/api/conferences/';

try{
    const response = await fetch(url);
    if (!response.ok){
        throw new Error ("Response not there");
    } else {
        const data = await response.json();
        // const conference = data.conferences[0];
        // const nameTag = document.querySelector('.card-title');
        // nameTag.innerHTML = conference.name;
        
        for (let conference of data.conferences){
        const detailUrl = `http://localhost:8000${conference.href}`;
        const detailResponse = await fetch(detailUrl);
        if (detailResponse.ok) {
            const details = await detailResponse.json();
            const title = details.conference.name;
            const description = details.conference.description;
            const pictureUrl = details.conference.location.picture_url;
            const location = details.conference.location.name;
            const startDate = details.conference.starts;
            const endDate = details.conference.ends;
            let d1 = new Date(startDate)
            let newStartDate = (d1.getMonth() + 1) + "/" + d1.getDate() + "/" + d1.getFullYear()
            let d2 = new Date(endDate)
            let newEndDate = (d2.getMonth() + 1) + "/" + d2.getDate() + "/" + d2.getFullYear()
            const html = createCard(title, description, pictureUrl, newStartDate, newEndDate, location);
            
            const row = document.querySelector('.row');
            row.innerHTML += html;
            // const describe = document.querySelector('.card-text');
            // describe.innerHTML = details.conference.description;
            // const imageTag = document.querySelector('.card-img-top')
            // imageTag.src = details.conference.location.picture_url;
        }
      }
    }
} catch (e) {
    const html = errorAlert();
    const col = document.querySelector('.row');
    col.innerHTML = html
    console.error("error", e)
}
});


